
""" 
This is an implementation of the Reverse Method of a LinkedList

Methods:

-- Append
-- Reverse Iteratively
-- Reverse Recursively
"""

class Node:

    def __init__(self, data=None):
        self.data = data
        self.next = None

    def __str__(self):
        return f"{self.data}"


class LinkedList:

    def __init__(self):
        self.head = None 
        self.tail = None

    
    def append_node(self, node):
        if not isinstance(node, Node):
            node = Node(node)

        if self.head == None:
            self.head = node 
        else:
            self.tail.next = node 
        
        self.tail = node


    def reverse_iterative(self):
       
        current_node = self.head 
        prev_node = None 

        while current_node:
            next_node = current_node.next 
            current_node.next = prev_node 
            prev_node = current_node
            current_node = next_node
            
        self.head = prev_node


    def reverse_recursive(self):
        pass

    
    def __str__(self):
        current_node = self.head 
        l_list = ""
        while current_node:
            l_list += str(current_node.data) + " -> "
            current_node = current_node.next 

        if l_list:
            return l_list[:-3]
        else:
            return l_list


sample_list = LinkedList()
sample_list.append_node("Python")
sample_list.append_node("JavaScript")
sample_list.append_node("C++")

print(sample_list)

sample_list.reverse_iterative()

print(sample_list)